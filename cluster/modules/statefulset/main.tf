resource "kubernetes_ingress_v1" "ingress" {
  metadata {
    name = "ingress-test"
    annotations = {
      "kubernetes.io/ingress.class" = "traefik"
    }
  }
  spec {
    rule {
      http {
        path {
          path = "/"
          path_type = "Prefix"
          backend {
            service {
              name = "sv-front"
              port {
                number = 80
              }
            }
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "sv3" {
  metadata {
    name = "sv-front"
  }
  spec {
    type = "ClusterIP"
    selector = {
      app = "front"
    }
    port {
      port = 80
      target_port = 3000
    }
  }
}

resource "kubernetes_deployment" "deployment2" {
  metadata {
    name = "deploy-front"
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "front"
      }
    }
    template {
      metadata {
        labels = {
          app = "front"
        }
      }
      spec {
        container {
          image = "registry.gitlab.com/alexgarden92160/terraform-gke/front:latest"
          image_pull_policy = "Always"
          name = "container-front"
          resources {
            requests = {
              cpu = "1m"
              memory = "100Mi"
             }
             limits = {
              cpu = "500m"
              memory = "1000Mi"
            }
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "sv2" {
  metadata {
    name = "sv-back"
  }
  spec {
    selector = {
      app = "back"
    }
    cluster_ip = "None"
    port {
      port = 3000
    }
  }
}

 resource "kubernetes_deployment" "deployment" {
   metadata {
     name = "deploy-back"
   }
   spec {
    replicas = 1
     selector {
       match_labels = {
         app = "back"
       }
     }
     template {
       metadata {
         labels = {
           app = "back"
         }
       }
       spec {
         container {
           image = "registry.gitlab.com/alexgarden92160/terraform-gke/back:latest"
           image_pull_policy = "Always"
           name = "container-back"
           resources {
             requests = {
               cpu = "1m"
               memory = "100Mi"
             }
             limits = {
               cpu = "500m"
               memory = "1000Mi"
             }
           }
           env {
             name = "MONGO_USERNAME"
             value_from {
               secret_key_ref {
                 name = "auth-db"
                 key = "username"
               }
             }
           }
           env {
             name = "MONGO_PASSWORD"
             value_from {
               secret_key_ref {
                 name = "auth-db"
                 key = "password"
               }
             }
           }
           env {
             name = "MONGO_URL"
             value_from {
               config_map_key_ref {
                 name = "info-db"
                 key = "url"
               }
             }
           }
         }
       }
     }
   }
 }

resource "kubernetes_cluster_role" "cr" {
  metadata {
    name = "cr-dev"
  }
  rule {
    api_groups = [""]
    resources = ["namespaces", "configmaps", "secrets", "deployments", "statefulsets", "services"]
    verbs = ["create", "update", "patch", "delete"]
  }
}

resource "kubernetes_cluster_role_binding" "crb" {
  metadata {
    name = "crb-dev"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind = "ClusterRole"
    name = "cr-dev"
  }
  subject {
    kind = "User"
    name = "dev-261@consummate-mark-374513.iam.gserviceaccount.com"
    api_group = "rbac.authorization.k8s.io"
  }
}

# resource "kubernetes_service_account" "sa" {
#   metadata {
#     name = "dev"
#   }
# }

resource "kubernetes_config_map" "cp" {
  metadata {
    name = "info-db"
  }
  data = {
    url = "sv-db"
  }
}

resource "kubernetes_secret" "secret" {
  metadata {
    name = "auth-db"
  }
  data = {
    username = "admin"
    password = "password"
  }
}

resource "kubernetes_storage_class" "sc" {
  metadata {
    name = "test-sc"
  }
  storage_provisioner = "pd.csi.storage.gke.io"
  parameters = {
    type = "pd-balanced"
  }
  reclaim_policy = "Retain"
  volume_binding_mode = "Immediate"
}

resource "kubernetes_service" "sv" {
  metadata {
    name = "sv-db"
  }
  spec {
    cluster_ip = "None"
    selector = {
      app = "db"
    }
    port {
      port = 27017
    }
  }
}

 resource "kubernetes_stateful_set" "sfs" {
   metadata {
     name = var.name
   }
   spec {
     service_name = "sv-db"
     selector {
       match_labels = {
         app = "db"
       }
     }
     template {
       metadata {
         labels = {
           app = "db"
         }
       }
       spec {
         container {
           name = "db-container"
           image = "mongo:latest"
           image_pull_policy = "Always"
           resources {
             requests = {
               cpu = "1m"
               memory = "100Mi"
             }
             limits = {
               cpu = "500m"
               memory = "1000Mi"
             }
           }
           env {
             name = "MONGO_INITDB_ROOT_USERNAME"
             value_from {
               secret_key_ref {
                 name = "auth-db"
                 key = "username"
               }
             }
           }
           env {
             name = "MONGO_INITDB_ROOT_PASSWORD"
             value_from {
               secret_key_ref {
                 name = "auth-db"
                 key = "password"
               }
             }
           }
           volume_mount {
             name = "data-db"
             mount_path = "/data/db"
           }
         }
       }
     }
     volume_claim_template {
       metadata {
         name = "data-db"
      }
      spec {
        access_modes = ["ReadWriteOnce"]
        storage_class_name = "test-sc"
        resources {
          requests = {
            storage = var.size
          }
        }
      }
    }
  }
}
