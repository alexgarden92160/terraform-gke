output "ns-name" {
  value = kubernetes_namespace.ns.metadata[0].name
}

output "ns-label" {
  value = kubernetes_namespace.ns.metadata[0].labels
}