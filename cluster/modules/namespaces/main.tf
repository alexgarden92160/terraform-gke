resource "kubernetes_namespace" "ns" {
  metadata {
    name = "${var.name}-${terraform.workspace}"
    labels = {
      "${var.label-key}" = var.label-value
      env = terraform.workspace
    }
  }
}