terraform {
  backend "kubernetes" {
    secret_suffix = "state"
    config_path = "~/.kube/config"
  }
}

variable "kube_conf_path" {}

provider "kubernetes" {
  config_path = var.kube_conf_path
}

provider "helm" {
  kubernetes {
    config_path = var.kube_conf_path
  }
}

resource "helm_release" "traefik" {
  name = "traefik"
  repository = "https://traefik.github.io/charts"
  chart = "traefik"
}