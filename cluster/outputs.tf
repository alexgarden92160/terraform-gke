output "summary" {
  value = {
    namespaces_project_name = module.namespaces_project.ns-name
    namespaces_project_label = module.namespaces_project.ns-label
  }
}