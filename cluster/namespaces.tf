module "namespaces_project" {
  source = "./modules/namespaces"
  name = "ns"
  label-key = "app"
  label-value = "project"
}

module "statefulset_project" {
  source = "./modules/statefulset"
  name = "sfs-db"
  size = "5Gi"
}