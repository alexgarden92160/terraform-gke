#!/bin/sh

while ! nc -z sv-back 3000 ; do
    echo "Waiting for the API Server..."
    sleep 3
done

echo "API Server online !"

npm start