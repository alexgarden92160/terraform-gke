#!/bin/sh

while ! nc -z ${process.env.MONGO_URL} 27017 ; do
    echo "Waiting for the Mongo Server..."
    sleep 3
done

echo "Mongo Server online !"

npm start